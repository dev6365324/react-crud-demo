import { faker } from '@faker-js/faker'

faker.seed(1)
faker.setDefaultRefDate(new Date('2024-01-01'))

// model User {
//   id        Int       @id @default(autoincrement())
//   email     String    @unique
//   password  String
//   name      String
//   gender    Int?
//   birthday  DateTime?
//   createdAt DateTime  @default(now())
//   updatedAt DateTime  @default(now()) @updatedAt
// }

const genderArr = [
  { index: 1, type: 'male' },
  { index: 2, type: 'female' },
]

function createRandomUser() {
  const genderObj = faker.helpers.arrayElement(genderArr)
  const firstName = faker.person.firstName(genderObj.type)
  const lastName = faker.person.lastName()
  const email = faker.internet.email({ firstName, lastName })

  return {
    email,
    password: faker.internet.password({ length: 8, memorable: true }),
    name: `${firstName} ${lastName}`,
    gender: genderObj.index,
    birthday: faker.date.birthdate({ min: 18, max: 65, mode: 'age' }),
  }
}

// model Product {
//   id                Int                 @id @default(autoincrement())
//   name              String
//   price             Decimal
//   createdAt         DateTime            @default(now())
//   updatedAt         DateTime            @default(now()) @updatedAt
//   Order_Product_Map Order_Product_Map[]
// }

function createRandomProuct() {
  return {
    name: faker.commerce.product(),
    price: faker.number.float({ min: 10, max: 1000, fractionDigits: 2 }),
  }
}

// model Order {
//   id                Int                 @id @default(autoincrement())
//   userId            Int
//   createdAt         DateTime            @default(now())
//   updatedAt         DateTime            @default(now()) @updatedAt
//   Order_Product_Map Order_Product_Map[]
// }

function createRandomOrder(userCount = 5) {
  return {
    userId: faker.number.int({ min: 1, max: userCount }),
  }
}

// model Order_Product_Map {
//   order     Order    @relation(fields: [orderId], references: [id])
//   orderId   Int
//   product   Product  @relation(fields: [productId], references: [id])
//   productId Int
//   qty       Int
//   createdAt DateTime @default(now())
//   updatedAt DateTime @default(now()) @updatedAt

//   @@id([orderId, productId])
// }

function createRandomOrderProductMap(orderCount = 5, productCount = 5) {
  const resultList = []
  const allProductList = Array(productCount)
    .fill(0)
    .map((item, i) => i + 1)

  for (let orderId = 1; orderId <= orderCount; orderId++) {
    const productList = faker.helpers.arrayElements(allProductList, {
      min: 1,
      max: 10,
    })
    resultList.push(
      ...productList.map((item) => ({
        orderId,
        productId: item,
        qty: faker.number.int({ min: 1, max: 30 }),
      }))
    )
  }

  return resultList
}

export {
  createRandomUser,
  createRandomProuct,
  createRandomOrder,
  createRandomOrderProductMap,
}
