import { PrismaClient } from '@prisma/client'
import {
  createRandomUser,
  createRandomProuct,
  createRandomOrder,
  createRandomOrderProductMap,
} from './faker.mjs'

const prisma = new PrismaClient()

const userCount = 50,
  productCount = 1000,
  orderCount = 500

async function main() {
  for (let i = 0; i < userCount; i++) {
    const user = await prisma.user.create({ data: createRandomUser() })
    // console.log(user)
  }

  for (let i = 0; i < productCount; i++) {
    const product = await prisma.product.create({ data: createRandomProuct() })
  }

  for (let i = 0; i < orderCount; i++) {
    const order = await prisma.order.create({
      data: createRandomOrder(userCount),
    })
  }

  const orderProductMap = await prisma.order_Product_Map.createMany({
    data: [...createRandomOrderProductMap(orderCount, productCount)],
  })
}

main()
  .then(async () => {
    await prisma.$disconnect()
  })
  .catch(async (e) => {
    console.error(e)
    await prisma.$disconnect()
    process.exit(1)
  })
