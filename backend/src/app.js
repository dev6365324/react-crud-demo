const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const cors = require('cors')

const routeSetup = require('./routes/routeSetup')

/**
 *
 * @param {*} flag environment: 'node' or 'lambda'
 */
const createApp = (flag) => {
  const app = express()

  if (flag === 'node') {
    app.use(logger('dev'))
  }

  if (flag === 'lambda') {
  }

  app.use(express.json())
  app.use(express.urlencoded({ extended: true }))
  app.use(cookieParser())
  // app.use(express.static(path.join(__dirname, '../public')))
  app.use(cors({ origin: 'http://localhost:9000' }))

  routeSetup(app)

  /* Error handler middleware */
  app.use((err, req, res, next) => {
    const statusCode = err.statusCode || 500
    console.error(err.message, err.stack)
    res.status(statusCode).json({ message: err.message })

    return
  })

  return app
}

module.exports = { createApp }
