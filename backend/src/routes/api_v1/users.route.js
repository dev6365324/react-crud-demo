const express = require('express')
const userController = require('../../controllers/users.controller')

const router = express.Router()

/* GET users listing. */
router.route('/users').get(function (req, res, next) {
  res.send('users!!')
})

router.route('/users/getUserInfo').get(userController.getUserInfo)

router.route('/users/getUserInfoDelay').get(userController.getUserInfoDelay)

router.route('/userList').get(userController.userList)

module.exports = router
