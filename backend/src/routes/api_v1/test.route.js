const express = require('express')
const sleep = require('../../utils/sleep')
const {
  nestException,
  nestException2,
} = require('../../controllers/test.controller')

const router = express.Router()

router.get('/test', function (req, res, next) {
  res.send('test index')
})

router.get('/test/exception', (req, res, next) => {
  throw new Error('exception')
  res.send('test async')
})

router.get('/test/async', async (req, res, next) => {
  await sleep()
  res.send('test async')
})

router.get('/test/asyncException', async (req, res, next) => {
  await sleep()
  throw new Error('asyncException')
  res.send('test async')
})

router.get('/test/asyncExceptionSafe', async (req, res, next) => {
  try {
    await sleep()
    throw new Error('asyncException')
    res.send('test async')
  } catch (err) {
    next(err)
  }
})

router.get('/test/nestAsyncException', async (req, res, next) => {
  await nestException()
  res.send('nestAsyncException')
})

router.get('/test/nestAsyncExceptionSafe', async (req, res, next) => {
  try {
    await nestException()
    res.send('nestAsyncException')
  } catch (err) {
    next(err)
  }
})

router.get('/test/nestAsyncException2', (req, res, next) => {
  nestException2()
  res.send('nestAsyncException')
})

module.exports = router
