const testRouter = require('./api_v1/test.route')
const indexRouter = require('./api_v1/index.route')
const usersRouter = require('./api_v1/users.route')

const routeSetup = (app) => {
  app.use('/api/v1', testRouter)
  app.use('/api/v1', indexRouter)
  app.use('/api/v1', usersRouter)
}

module.exports = routeSetup
