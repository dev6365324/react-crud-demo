const sleep = require('../utils/sleep')
const prisma = require('../utils/db.js')

const getUserInfo = () => {
  return `user info, ${new Date().toLocaleString()}`
}

const getUserInfoDelay = async (time = 1000) => {
  await sleep(time)
  return `user info delay ${time}ms, ${new Date().toLocaleString()}`
}

const userList = async ({
  pageSize = 10,
  page = 0,
  sortModel = {},
  filterModel = [],
}) => {
  pageSize = isNaN(parseInt(pageSize)) ? 10 : parseInt(pageSize)
  page = isNaN(parseInt(page)) ? 0 : parseInt(page)

  const options = {
    skip: pageSize * page,
    take: pageSize,
  }

  if (sortModel.field && sortModel.sort) {
    options.orderBy = [
      {
        [sortModel.field]: sortModel.sort,
      },
    ]
  }

  let where = {}
  if (filterModel && Array.isArray(filterModel)) {
    filterModel = filterModel.filter(({ field, value }) => field && value)
    if (filterModel.length > 0) {
      filterModel.forEach(
        (model) => (where[model.field] = { contains: model.value })
      )
    }
  }

  options.where = where
  console.log('options', options)
  const [count, records] = await Promise.all([
    prisma.user.count({ where }),
    prisma.user.findMany(options),
  ])
  return { count, records }
}

module.exports = { getUserInfo, getUserInfoDelay, userList }
