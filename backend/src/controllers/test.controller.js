const sleep = require('../utils/sleep')

const nestException = async () => {
  await sleep()
  throw new Error('nestException')
}

const nestException2 = () => {
  const fn = async () => {
    await sleep()
    throw new Error('nestException')
  }
  fn()
}

module.exports = {
  nestException,
  nestException2,
}
