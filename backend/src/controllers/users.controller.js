const userService = require('../services/users.service.js')

const getUserInfo = (req, res, next) => res.send(userService.getUserInfo())

const getUserInfoDelay = async (req, res, next) => {
  const time = req.query.time
  res.send(await userService.getUserInfoDelay(time))
}

const userList = async (req, res, next) => {
  try {
    console.log('query ', req.query)
    res.send(await userService.userList(req.query))
  } catch (error) {
    res.status(500).send({ error: error.message })
  }
}

module.exports = {
  getUserInfo,
  getUserInfoDelay,
  userList,
}
