const sleep = (time = 1000) =>
  new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(null)
    }, time)
  })

module.exports = sleep
