/*
  Warnings:

  - You are about to drop the column `amount` on the `Order_Product_Map` table. All the data in the column will be lost.
  - Added the required column `qty` to the `Order_Product_Map` table without a default value. This is not possible if the table is not empty.

*/
-- RedefineTables
PRAGMA defer_foreign_keys=ON;
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Order_Product_Map" (
    "orderId" INTEGER NOT NULL,
    "productId" INTEGER NOT NULL,
    "qty" INTEGER NOT NULL,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,

    PRIMARY KEY ("orderId", "productId"),
    CONSTRAINT "Order_Product_Map_orderId_fkey" FOREIGN KEY ("orderId") REFERENCES "Order" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "Order_Product_Map_productId_fkey" FOREIGN KEY ("productId") REFERENCES "Product" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_Order_Product_Map" ("createdAt", "orderId", "productId", "updatedAt") SELECT "createdAt", "orderId", "productId", "updatedAt" FROM "Order_Product_Map";
DROP TABLE "Order_Product_Map";
ALTER TABLE "new_Order_Product_Map" RENAME TO "Order_Product_Map";
PRAGMA foreign_keys=ON;
PRAGMA defer_foreign_keys=OFF;
