# Material UI - Next.js App Router example

This is a [Next.js](https://nextjs.org/) project bootstrapped using [`create-next-app`](https://github.com/vercel/next.js/tree/HEAD/packages/create-next-app) with Material UI installed.

Source code : [github](https://github.com/mui/material-ui/tree/next/examples/material-ui-nextjs)

## How to use

Download the example [or clone the repo](https://github.com/mui/material-ui):

<!-- #default-branch-switch -->

```bash
curl https://codeload.github.com/mui/material-ui/tar.gz/master | tar -xz --strip=2  material-ui-master/examples/material-ui-nextjs
cd material-ui-nextjs
```

Install it and run:

```bash
npm install
npm run dev
```

Open [http://localhost:9000](http://localhost:9000) with your browser to see the result.
