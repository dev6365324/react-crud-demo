import * as React from 'react'
import Container from '@mui/material/Container'
import Typography from '@mui/material/Typography'
import Box from '@mui/material/Box'

export default function Home() {
  return (
    <Container maxWidth='lg'>
      <Box
        sx={{
          my: 4,
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Typography variant='h4' component='h1' sx={{ mb: 2 }}>
          Material UI - Next.js example
        </Typography>
        <Typography variant='h5' component='h5' sx={{}}>
          Home Page
        </Typography>
      </Box>
    </Container>
  )
}
