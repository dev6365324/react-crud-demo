import React from 'react'

import CRUDtab from '@/components/user/CRUDtab'

function Page() {
  return (
    <>
      <h2>User Page</h2>
      <CRUDtab></CRUDtab>
    </>
  )
}

export default Page
