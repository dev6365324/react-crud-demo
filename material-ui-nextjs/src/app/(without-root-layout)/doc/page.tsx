import React from 'react'
import MarkdownContent from '@/components/Markdown/MarkdownContent'

function Page() {
  if (process.env.NODE_ENV == 'development') {
    return <MarkdownContent />
  }
  return null
}

export default Page
