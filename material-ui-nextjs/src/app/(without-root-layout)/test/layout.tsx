import React from 'react'
import Link from 'next/link'
import { Box } from '@mui/material'
import UserInfo from '@/components/test/UserInfo'

function Layout({ children }: { children: React.ReactNode }) {
  console.log('layout(test) rendered')

  return (
    <>
      <Box
        sx={{
          mb: 4,
          p: 4,
          borderBottom: '1px solid lightgrey',
        }}
      >
        <div>
          <Link href='/test/tab1'>Tab 1</Link>
        </div>
        <div>
          <Link href='/test/tab2'>Tab 2</Link>
        </div>
        {/* <UserInfo /> */}
      </Box>
      <div>{children}</div>
    </>
  )
}

export default Layout
