'use client'

import UserInfo from '@/components/test/UserInfo'
import { useTestValue } from '@/components/Providers/TestProvider'

function Page() {
  const { testValue, setTestValue } = useTestValue() as any

  console.log('tab 1 rendered')

  return (
    <div>
      <div>tab 1</div>
      <div>test value: {testValue.toString()}</div>
      <div>
        <button onClick={() => setTestValue((value: number) => ++value)}>
          +1
        </button>
      </div>
      <UserInfo />
    </div>
  )
}

export default Page
