// import UserInfo from '@/components/test/UserInfo'
import ShowTime from '@/components/test/ShowTime'
import RefreshBtn from '@/components/test/RefreshBtn'

// export const dynamic = 'force-dynamic'

const Div = ({ children }: { children: React.ReactNode }) => {
  console.log('Div rendered')
  return <div>{children}</div>
}

function Page() {
  console.log('tab 2 rendered')

  return (
    <div>
      <div>tab 2</div>
      {/* <UserInfo /> */}
      <ShowTime></ShowTime>
      <RefreshBtn></RefreshBtn>
    </div>
  )
}

// export default Page

export default () => (
  <Div>
    <Page></Page>
  </Div>
)
