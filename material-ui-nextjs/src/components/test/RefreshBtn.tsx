'use client'

import React from 'react'
import { useRouter } from 'next/navigation'

function RefreshBtn() {
  const router = useRouter()

  return (
    <>
      <div>RefreshBtn</div>
      <div>
        <button onClick={() => router.refresh()}>refresh</button>
      </div>
    </>
  )
}

export default RefreshBtn
