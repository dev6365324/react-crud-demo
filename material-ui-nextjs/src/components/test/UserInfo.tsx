'use client'

import { Box } from '@mui/material'
import useSWR from 'swr'

function UserInfo() {
  const { data, error, isLoading } = useSWR(
    'http://localhost:9001/api/v1/users/getUserInfo',
    (url) => fetch(url).then((res) => res.text())
  )

  return (
    <Box sx={{ m: 2, '& > *': { m: 1 } }}>
      <div>UserInfo</div>
      <div>{`data: ${data}`}</div>
      <div>{`error: ${error}`}</div>
      <div>{`isLoading: ${isLoading}`}</div>
    </Box>
  )
}

export default UserInfo
