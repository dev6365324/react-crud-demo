import React from 'react'

const getTime = (): Promise<string> =>
  new Promise((res, rej) => res(new Date().toLocaleString()))

async function ShowTime() {
  const time = await getTime()

  return (
    <>
      <div>ShowTime:</div>
      <div>{time}</div>
    </>
  )
}

export default ShowTime
