'use client'

import React from 'react'
import { Box, Tabs, Tab } from '@mui/material'
import TabContentRead from './TabContentRead'
import TabContentCreate from './TabContentCreate'
import TabContentUpdate from './TabContentUpdate'
import TabContentDelete from './TabContentDelete'

const TabPanelContent = React.memo(function ({
  Content,
}: {
  Content: React.FC
}) {
  // console.log('TabPanelContent')
  return <>{Content ? <Content /> : null}</>
})

function TabPanel(props: {
  children?: React.ReactNode
  value: number
  index: number
  Content: React.FunctionComponent
}) {
  const { children, value, index, Content, ...other } = props

  return (
    <div
      role='tabpanel'
      hidden={value !== index}
      id={`tabpanel-${index}`}
      aria-labelledby={`tab-${index}`}
      {...other}
    >
      {/* {value === index && <Box sx={{ p: 3 }}>{children}</Box>} */}

      {/* {value === index ? (
        <Box sx={{ p: 3 }}>
          <TabPanelContent Content={Content}></TabPanelContent>
        </Box>
      ) : null} */}

      <Box sx={{ p: 3 }}>
        <TabPanelContent Content={Content}></TabPanelContent>
      </Box>
    </div>
  )
}

// export function TabPanelRead() {
//   console.log('TabPanelRead')
//   return <>Read</>
// }

// export function TabPanelCreate() {
//   return <>Create</>
// }

// export function TabPanelUpdate() {
//   return <>Update</>
// }

// export function TabPanelDelete() {
//   return <>Delete</>
// }

function CRUDtab() {
  const [value, setValue] = React.useState(0)

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue)
  }

  return (
    <>
      <Box>
        <Tabs
          variant='scrollable'
          value={value}
          onChange={handleChange}
          sx={{ borderBottom: 1, borderColor: 'divider' }}
        >
          <Tab label='Read' />
          <Tab label='Create' />
          <Tab label='Update' />
          <Tab label='Delete' />
        </Tabs>

        <TabPanel value={value} index={0} Content={TabContentRead} />
        <TabPanel value={value} index={1} Content={TabContentCreate} />
        <TabPanel value={value} index={2} Content={TabContentUpdate} />
        <TabPanel value={value} index={3} Content={TabContentDelete} />
      </Box>
    </>
  )
}

export default CRUDtab
