import React from 'react'
import 'github-markdown-css/github-markdown.css'
import { Box } from '@mui/material'

function MarkdownContainer({ content }: { content: string }) {
  return (
    <Box
      sx={{
        margin: '0 auto',
        minWidth: '200px',
        maxWidth: '980px',
        padding: { sm: '15px', md: '45px' },
      }}
    >
      <div
        className='markdown-body'
        dangerouslySetInnerHTML={{ __html: content }}
      ></div>
    </Box>
  )
}

export default MarkdownContainer
