import path from 'node:path'
import fs from 'node:fs/promises'
import React from 'react'
import Showdown from 'showdown'
import MarkdownContainer from '@/components/Markdown/MarkdownContainer'

const convertReadme = async () => {
  try {
    const data = await fs.readFile(path.resolve('./', 'README.md'), {
      encoding: 'utf8',
    })
    // console.log(data)

    const converter = new Showdown.Converter()
    converter.setFlavor('github')
    const html = converter.makeHtml(data)
    // console.log('html', html)
    return html
  } catch (err) {
    console.log(err)
    if (err instanceof Error) return err.message
    return String(err)
  }
}

async function MarkdownContent() {
  const content = await convertReadme()
  return <MarkdownContainer content={content} />
}

export default MarkdownContent
