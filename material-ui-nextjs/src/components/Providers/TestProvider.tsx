import React from 'react'

const TestContext = React.createContext({})

export const useTestValue = () => {
  const context = React.useContext(TestContext)
  if (!context) {
    throw new Error('useTestValue must be used within TestProvider')
  }
  return context
}

export function TestProvider({
  children,
  value = 0,
}: {
  children: React.ReactNode
  value?: string | number
}) {
  value = isNaN(parseInt(value.toString())) ? 0 : parseInt(value.toString())
  const [testValue, setTestValue] = React.useState(value)

  console.log('TestProvider rendered')

  return (
    <>
      <TestContext.Provider value={{ testValue, setTestValue }}>
        {children}
      </TestContext.Provider>
    </>
  )
}
