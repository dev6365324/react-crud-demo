'use client'

import React from 'react'

import { AppRouterCacheProvider } from '@mui/material-nextjs/v14-appRouter'
import { ThemeProvider } from '@emotion/react'
import theme from '@/theme'
import { TestProvider } from '@/components/Providers/TestProvider'

function RootProvider({ children }: { children: React.ReactNode }) {
  return (
    <>
      <AppRouterCacheProvider options={{ enableCssLayer: true }}>
        <ThemeProvider theme={theme}>
          <TestProvider>{children}</TestProvider>
        </ThemeProvider>
      </AppRouterCacheProvider>
    </>
  )
}

export default RootProvider
