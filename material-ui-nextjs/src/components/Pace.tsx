'use client'

import { useTheme } from '@mui/material/styles'
import Script from 'next/script'
import '@/../public/pace/pace.css'

function Pace() {
  const theme = useTheme()

  return (
    <>
      <Script
        src='/pace/pace.js'
        onReady={() => {
          const primaryColor = theme.palette.primary.main
          // const secondaryColor = theme.palette.secondary.main
          const paceProgressDiv = document.querySelector('.pace .pace-progress') as HTMLElement
          // console.log('paceProgressDiv', paceProgressDiv)
          if (paceProgressDiv) {
            paceProgressDiv.style.backgroundColor = primaryColor
          }
        }}
      />
    </>
  )
}

export default Pace
