'use client'

import * as React from 'react'
import Link from 'next/link'
import AppBar from '@mui/material/AppBar'
import Box from '@mui/material/Box'
import Divider from '@mui/material/Divider'
import Drawer from '@mui/material/Drawer'
import IconButton from '@mui/material/IconButton'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemText from '@mui/material/ListItemText'
import MenuIcon from '@mui/icons-material/Menu'
import Toolbar from '@mui/material/Toolbar'
import Typography from '@mui/material/Typography'
import Button from '@mui/material/Button'

const drawerWidth = 240
const navItems = [
  { name: 'Home', path: '/' },
  { name: 'User', path: '/user' },
  { name: 'Product', path: '/product' },
  { name: 'Order', path: '/order' },
  { name: 'About', path: '/about' },
  { name: 'Test', path: '/test' },
]

function DrawerAppBar({ children }: { children: React.ReactNode }) {
  const [mobileOpen, setMobileOpen] = React.useState(false)

  const handleDrawerToggle = () => {
    setMobileOpen((prevState) => !prevState)
  }

  const drawer = (
    <Box onClick={handleDrawerToggle} sx={{ textAlign: 'center' }}>
      <Typography variant='h6' sx={{ my: 2, color: 'primary.main' }}>
        MUI
      </Typography>
      <Divider />
      <List>
        {navItems.map((item) => (
          <ListItem key={item.name} disablePadding>
            <ListItemButton sx={{ textAlign: 'center' }}>
              <ListItemText
                primary={
                  <Link
                    href={item.path}
                    style={{ textDecoration: 'none', color: 'inherit' }}
                  >
                    {item.name}
                  </Link>
                }
              />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    </Box>
  )

  return (
    <Box>
      <AppBar component='nav' color='inherit' elevation={1} variant='outlined'>
        <Toolbar sx={{ minHeight: 64 }}>
          <IconButton
            color='inherit'
            aria-label='open drawer'
            edge='start'
            onClick={handleDrawerToggle}
            sx={{ mr: 2, display: { sm: 'none' } }}
          >
            <MenuIcon />
          </IconButton>
          <Typography
            variant='h6'
            component='div'
            // sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block' } }}
            sx={{ flexGrow: 1, color: 'primary.main' }}
          >
            MUI
          </Typography>
          <Box sx={{ display: { xs: 'none', sm: 'block' } }}>
            {navItems.map((item) => (
              <Button key={item.name}>
                {
                  <Link
                    href={item.path}
                    style={{ textDecoration: 'none', color: 'inherit' }}
                  >
                    {item.name}
                  </Link>
                }
              </Button>
            ))}
          </Box>
        </Toolbar>
      </AppBar>

      <Drawer
        variant='temporary'
        open={mobileOpen}
        onClose={handleDrawerToggle}
        ModalProps={{
          keepMounted: true, // Better open performance on mobile.
        }}
        sx={{
          display: { xs: 'block', sm: 'none' },
          '& .MuiDrawer-paper': {
            boxSizing: 'border-box',
            width: drawerWidth,
          },
        }}
      >
        {drawer}
      </Drawer>

      <Box component='main' sx={{ p: 3, marginTop: '64px' }}>
        <Box sx={{ minHeight: 1000 }}>{children}</Box>
      </Box>
    </Box>
  )
}

export default DrawerAppBar
