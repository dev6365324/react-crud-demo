import * as React from 'react'
import CssBaseline from '@mui/material/CssBaseline'
import DrawerAppBar from '@/components/DrawerAppBar'
import Pace from '@/components/Pace'
import RootProvider from './Providers/RootProvider'

export default function RootLayout({
  children,
  withAppBar,
}: {
  children: React.ReactNode
  withAppBar?: boolean
}) {
  const AppBar = ({ children }: { children: React.ReactNode }) =>
    withAppBar ? <DrawerAppBar>{children}</DrawerAppBar> : <>{children}</>

  return (
    <html lang='en'>
      <body>
        <Pace />
        <RootProvider>
          {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
          <CssBaseline />
          <AppBar>{children}</AppBar>
        </RootProvider>
      </body>
    </html>
  )
}
